package camt.se234.project.service;

import camt.se234.project.dao.OrderDao;
import camt.se234.project.entity.Product;
import camt.se234.project.entity.SaleOrder;
import camt.se234.project.entity.SaleTransaction;
import org.hibernate.criterion.Order;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SaleOrderServiceImplTest {

        OrderDao orderDao;
        SaleOrderServiceImpl saleOrderService;

        @Before
    public void setup(){
            orderDao = mock(OrderDao.class);
            saleOrderService = new SaleOrderServiceImpl();
            saleOrderService.setOrderDao(orderDao);
        }

        @Test
    public void TestgetSaleOrders(){
            List<SaleTransaction> mockTransactions1 = new ArrayList<>();
            List<SaleTransaction> mockTransactions2 = new ArrayList<>();


            mockTransactions1.add(new SaleTransaction("T1", new SaleOrder("O1", mockTransactions1),
                    new Product(112L, "112", "Apple", "Can eat", "123.xxx", 1000.0), 10));
            mockTransactions1.add(new SaleTransaction("T2", new SaleOrder("O1", mockTransactions1),
                    new Product(112L,"113", "Banana", "Can eat", "124.xxx", 500.0), 5));
            mockTransactions2.add(new SaleTransaction("T3", new SaleOrder("O2", mockTransactions2),
                    new Product(113L,"114", "CAT", "Can eat?", "125.xxx", 10.25), 10));
            mockTransactions2.add(new SaleTransaction("T4", new SaleOrder("O2", mockTransactions2),
                    new Product(114L,"114", "Dog", "Can eat?", "126.xxx", 1.0), 6));
            List<SaleOrder> mockOrder = new ArrayList<>();

            mockOrder.add(new SaleOrder("O1", mockTransactions1));
            mockOrder.add(new SaleOrder("O2", mockTransactions2));

            when(orderDao.getOrders()).thenReturn(mockOrder);
            assertThat(saleOrderService.getSaleOrders(),hasItems(new SaleOrder("O1", mockTransactions1),new SaleOrder("O2" , mockTransactions2)));
        }

        @Test
    public void TestgetAverageSaleOrderPrice(){
            List<SaleTransaction> mockTransactions1 = new ArrayList<>();
            List<SaleTransaction> mockTransactions2 = new ArrayList<>();


            mockTransactions1.add(new SaleTransaction("T1", new SaleOrder("O1", mockTransactions1),
                    new Product(112L, "112", "Apple", "Can eat", "123.xxx", 10.0), 10));
            mockTransactions1.add(new SaleTransaction("T2", new SaleOrder("O1", mockTransactions1),
                    new Product(112L,"113", "Banana", "Can eat", "124.xxx", 50.0), 5));
            mockTransactions2.add(new SaleTransaction("T3", new SaleOrder("O2", mockTransactions2),
                    new Product(113L,"114", "CAT", "Can eat?", "125.xxx", 10.0), 10));
            mockTransactions2.add(new SaleTransaction("T4", new SaleOrder("O2", mockTransactions2),
                    new Product(114L,"114", "Dog", "Can eat?", "126.xxx", 100.0), 6));
            List<SaleOrder> mockOrder = new ArrayList<>();

            mockOrder.add(new SaleOrder("O1", mockTransactions1));
            mockOrder.add(new SaleOrder("O2", mockTransactions2));


            when(orderDao.getOrders()).thenReturn(mockOrder);
            assertThat(saleOrderService.getAverageSaleOrderPrice(), closeTo(525, 0.01));


        }


}
