package camt.se234.project.service;

import camt.se234.project.dao.ProductDao;
import camt.se234.project.entity.Product;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.nullValue;

public class ProductServiceImplTest {

    ProductDao productDao;
    ProductServiceImpl productService;

    @Before
    public void setup(){
        productDao = mock(ProductDao.class);
        productService = new ProductServiceImpl();
        productService.setProductDao(productDao);
    }

    @Test
    public void TestgetAllProduct(){
        List<Product> mockProduct = new ArrayList<>();
        productService.setProductDao(productDao);
        mockProduct.add(Product.builder().id(112L).productId("112").name("SeerO").description("Figure").imageLocation("www.10.com").price(235.0).build());
        mockProduct.add(Product.builder().id(113L).productId("113").name("Sutita").description("Figure").imageLocation("www.11.com").price(20.0).build());
        mockProduct.add(Product.builder().id(114L).productId("114").name("Pungpon").description("Figure").imageLocation("www.12.com").price(0.2).build());
        when(productDao.getProducts()).thenReturn(mockProduct);
        assertThat(productService.getAllProducts(),contains(mockProduct.toArray()));
    }

    @Test
    public void TestgetAvailableProducts(){
        List<Product> mockProduct = new ArrayList<>();
        productService.setProductDao(productDao);
        mockProduct.add(Product.builder().id(112L).productId("112").name("SeerO").description("Figure").imageLocation("www.10.com").price(235.0).build());
        mockProduct.add(Product.builder().id(113L).productId("113").name("Sutita").description("Figure").imageLocation("www.11.com").price(20.0).build());
        mockProduct.add(Product.builder().id(114L).productId("114").name("Pungpon").description("Figure").imageLocation("www.12.com").price(0.0).build());
        when(productDao.getProducts()).thenReturn(mockProduct);
        assertThat(productService.getAvailableProducts(),hasItems(new Product(112L,"112","SeerO","Figure","www.10.com",235.0)
                ,new Product(113L,"113","Sutita","Figure","www.11.com",20.0)));

    }

    @Test
    public void TestgetUnavailableProductSize(){
        List<Product> mockProduct = new ArrayList<>();
        productService.setProductDao(productDao);
        mockProduct.add(Product.builder().id(112L).productId("112").name("SeerO").description("Figure").imageLocation("www.10.com").price(235.0).build());
        mockProduct.add(Product.builder().id(113L).productId("113").name("Sutita").description("Figure").imageLocation("www.11.com").price(20.0).build());
        mockProduct.add(Product.builder().id(114L).productId("114").name("Pungpon").description("Figure").imageLocation("www.12.com").price(0).build());
        when(productDao.getProducts()).thenReturn(mockProduct);
        assertThat(productService.getUnavailableProductSize(),is(1));
    }

}
