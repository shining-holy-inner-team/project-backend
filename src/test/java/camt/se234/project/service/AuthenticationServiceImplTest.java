package camt.se234.project.service;

import camt.se234.project.dao.UserDao;
import camt.se234.project.dao.UserDaoImpl;
import camt.se234.project.entity.User;
import camt.se234.project.service.AuthenticationServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.nullValue;

public class AuthenticationServiceImplTest {

    UserDao userDao;
    AuthenticationServiceImpl authenticaionService;

    @Before
    public void setup(){
        userDao = mock(UserDao.class);
        authenticaionService = new AuthenticationServiceImpl();
        authenticaionService .setUserDao(userDao);
    }

    @Test
    public void authenticateTest(){
        List<User> mockUser = new ArrayList<>();
        authenticaionService.setUserDao(userDao);
        mockUser.add(User.builder().id(112L).username("O").password("Justin112").role("King").build());
        when(userDao.getUser("O","Justin112")).thenReturn(new User(112L,"O","Justin112","King"));
        assertThat(authenticaionService.authenticate("O","Justin112"),is(new User(112L,"O","Justin112","King")));
        assertThat(authenticaionService.authenticate("SutiTa","Exgirl"),nullValue());
    }
}
